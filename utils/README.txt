This directory contains useful tools related to mini-ramses, mostly for preparing ICs and postprocessing.
Add tools from the original RAMSES utils here once they have been adapted to the new RAMSES data structure.
